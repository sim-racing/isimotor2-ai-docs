# isiMotor2 Community AI Documentation Website

[https://sim-racing.gitlab.io/isimotor2-ai-docs/](https://sim-racing.gitlab.io/isimotor2-ai-docs/)

## About

This is a community documentation website to provide an up-to-date all-in-one source of information and guidance about the AI of sim racing titles built on isiMotor2 – most of all, [rFactor 1](https://store.steampowered.com/app/339790/rFactor/), [GTR 2](https://store.steampowered.com/app/8790/GTR_2_FIA_GT_Racing_Game/), and [Automobilista 1](https://store.steampowered.com/app/431600/automobilista). 

For years, information about AI settings and parameters has been scattered across PDF documents and forum threads or hidden in the heads of experienced modders. This site hopes to change this by having as much knowledge as possible in a single easy-to-access location.

## WIP

Please note that **this site is a work-in-progress**. Many pages on the site are currently incomplete, and more pages will likely need to be created.

## Contributing

Community fixes, improvements, additions, and suggestions are welcome! That's the beauty of an open-source website like this one. We'd love to have you share your feedback or chip in.

There are a few ways to do this:

- [Open an issue](https://gitlab.com/sim-racing/isimotor2-ai-docs/-/issues) on [GitLab](https://gitlab.com/sim-racing/isimotor2-ai-docs/)
- [Merge request](https://gitlab.com/sim-racing/isimotor2-ai-docs/-/merge_requests) on [GitLab](https://gitlab.com/sim-racing/isimotor2-ai-docs/)
- Post on [RaceDepartment](https://www.racedepartment.com/) in the rFactor 1, GTR 2, or Automobilista 1 forums and @ the site maintainer, MJQT
- Send MJQT a direct message on [RaceDepartment](https://www.racedepartment.com/)

## Thanks, acknowledgements

A massive thank you to everyone listed below!

- The [RaceDepartment](https://www.racedepartment.com/) community, including:
    - MJQT (primary author, researcher, repo maintainer)
    - Shovas
    - jgf
    - Speednut357
    - AndreasFSC
    - Bjarne Hansen
    - GTR233
    - Bela Valko
    - Salvatore Sirignano
    - Maxell
    - Lorencini
- Bram Hengeveld and the rest of the [RaceDepartment](https://www.racedepartment.com/) staff
- The NoGripRacing community
    - Von Dutch
    - Chronus (GTR 2 Reborn)
    - And many others...
- Austin Ogonoski
- The developers of these great driving and racing sims!
    - Image Space Incorporated
    - SimBin Studios
    - Reiza Studios
- The developers of the open-source tools used to build this site!
    - Python
    - MkDocs
    - Material for MkDocs
