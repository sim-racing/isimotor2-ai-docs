# Setups for AI

## Why create setups for AI?

TO BE COMPLETED



## Where to put AI setups? How to name them?

### rFactor 1

In [this RaceDepartment post](https://www.racedepartment.com/threads/ai-editing-the-strength-of-a-mod-need-advice.228552/post-3520392), jgf explains:

> Give the AI setups for each car and track; first set `Fixedsetups=1` and `FixedAIsetups=1` in your PLR file. I use my own setups but with top gear extended a click or two (otherwise the AI are likely to blow the engine). For example, you create a setup for your Mustang at Goodwood, set the top gear a little higher and save with different name; copy that setup (svm file) to the root folder of that Mustang (where the hdv and pm files are located) and rename it `Goodwood.svm`, now any AI driving that car at that track will use that setup. Look in the track's GBR file for `AISetting` for the proper name for the setup, otherwise it will be ignored.

### SimBin sims: GTR 2, RACE 07

TO BE COMPLETED

### Automobilista 1

The[^1] easiest way to edit/assign AI settings in AMS1 is to actually:

1. Copy one of your own settings (`.svm` file in your `UserData/<my_user>/settings` folder)
2. You have to paste that same settings file into the `GameData/Vehicles/<car_your_gonna_race_with>/`
3. You have to name the file as per the `SettingsFolder` in the `<selected_track>.gdb` file.

For example, suppose I want to assign a specific setup to Copa Marcas for the Macau mod track.

1. At the end of the `Macau_GP.gdb` file, I can see the following line: `SettingsFolder = Macau`. So my AI setup file will have to named: `Macau.svm`.
2. Now I can use any setup file I want (copy of my own for example) and paste that file in the `\Automobilista\GameData\Vehicles\Marcas` folder. Generally, it should be placed where the car's `gears.ini` file is located.
3. Make sure the file is named `Macau.svm`.

The AI will use that setup for the whole weekend. You can also assign a setup for quali and a different setup for pratice/race:

1. Name the quali setup: `<selected_track>.svm` (e.g. `Macau.svm`)
2. Name the pratice/race setup: `<selected_track>.race.svm` (e.g. `Macau.race.svm`)

Keep in mind that AI uses simplified physics. It seems there's only use in modifying gear ratios, wings, brakes and ride height. No need to tweak other settings (simplified physics) or pit stops/fuel (AI uses its own calculation).



[^1]: References for the AMS 1 section: [https://www.racedepartment.com/threads/portland-2-seasons-2-layouts.123092/post-2336177](https://www.racedepartment.com/threads/portland-2-seasons-2-layouts.123092/post-2336177), [https://www.racedepartment.com/threads/ai-using-setup.134539/post-2452833](https://www.racedepartment.com/threads/ai-using-setup.134539/post-2452833), [https://www.racedepartment.com/threads/how-to-make-ai-use-your-own-car-setups.88798/post-2139724](https://www.racedepartment.com/threads/how-to-make-ai-use-your-own-car-setups.88798/post-2139724) (for GSCe but AMS1 is basically the same engine)
