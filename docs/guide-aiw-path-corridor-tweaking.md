# Guide - AIW path, corridor tweaking



## Tools

### rFactor AIW/CAM Editor

This is a first-party tool provided by ISI to rFactor 1 modders. It allows you to load an rF1 track and edit the AIW and CAM files while being able to see the track mod in-game. You can drive and record a new fast path, visually see and tweak the position of the AI corridors relative to the track surface and runoff, drive and set garage positions, drive and set teleport positions, and more.

It sadly does not run on Windows 10 or 11, and there are mixed reports of whether it runs on Windows 7 today. [Simon Aguirre reports](https://www.racedepartment.com/threads/mid-ohio-aiw.216339/#post-3508627) that it *can* however be run on a Linux distribution through Wine.

To get an idea how to use the tool, please see this tutorial video from Madcowie (originally recorded *many* years ago):

<div class="video-wrapper">
  <iframe width="800" height="400" src="https://www.youtube.com/embed/lo3vEH1rXkU" frameborder="0" allowfullscreen></iframe>
</div>

freew67 also made two tutorial YouTube videos about how to use the ISI tool. [The first](https://youtu.be/vi4s7-KT53M) shows you the basics of AIW editing, then [the second](https://youtu.be/7dg6sK_KC_o) shows you other AIW tricks and the basics of creating cams for a track.

### guitarmaen AIWCAM Editor

[Click here](https://www.racedepartment.com/threads/wp_cheat.212452/#post-3461898) for a basic usage guide from tireless RACE 07 modder AndreasFSC.

TO BE COMPLETED




## Tips

### Fast path

**Do not use only one mod** to calibrate the location of the AIW fast path relative to off-track areas and walls. What works for one car may not work for another! When an AI races a car that is wider or longer, carnage may ensue! rFactor 2 developers appear to have made this mistake in the past, and you can see the results in the video below.

<div class="video-wrapper">
  <iframe width="1200" height="320" src="https://www.youtube.com/embed/mfzYZ4_6vxo" frameborder="0" allowfullscreen></iframe>
</div>

Be sure that the AIW fast path joins and is **smooth at the start/finish line**. Otherwise, the AI will slow down at the start/finish, as they will think there's a corner they need to take. rFactor 2 developers appear to have made this mistake when they released the first version of their DLC Daytona oval, and you can see the frustrating resulting AI behaviour at the start/finish in the video below.

<div class="video-wrapper">
  <iframe width="1200" height="320" src="https://www.youtube.com/embed/5XBFW60PK2Q" frameborder="0" allowfullscreen></iframe>
</div>


### Corridors

#### First corner funnel

> Speednut357 writes: "I have tried to make it like a funnel to give the AI a smooth transition into the turn, and greatly reduce their options for overtaking. I also removed the left-right swerve before the corner as it is no longer needed. From my testing, I found that by moving the track boundary in on the exit until it is very close to the racing line the AI will slow down. Basically the fewer options they have the more they will do what I want.".[^1]

#### Smooth corridors

> Speednut357 says: "I found that any kinks in the track boundary have a big effect on the AI. They seem to think they can use areas like this to overtake."[^2]

#### Avoid overtake stacking

"Overtake stacking" is a term coined by Austin Ogonoski to describe a peculiarity in sim racing AI overtaking behaviour (both in isiMotor2 and other game engines):

<div class="video-wrapper">
  <iframe width="1200" height="420" src="https://www.youtube.com/embed/n_n9Ibku6ho" frameborder="0" allowfullscreen></iframe>
</div>

Happily, the issue can be avoided! Here's a video from Austin explaining what needs to be done.

<div class="video-wrapper">
  <iframe width="1200" height="420" src="https://www.youtube.com/embed/TwDG_YdnEqM" frameborder="0" allowfullscreen></iframe>
</div>

Here is an example[^3] showing how to modify the AI corridors in an isiMotor2 AIW file to avoid overtake stacking into a tight corner using the guitarmaen AIW editor. The fast path is green, the corridors are red, the cut detection track limits (hidden) are green, and the far edges are brown.

![Image](./images/bucharest-ring-corridors-narrowed-overtake-stacking.jpeg)

#### Ensure corridors do NOT include non-raceable tarmac areas

Auto-generated corridors in a mod track's AIW file tend to follow the edges of the tarmac run-off around a circuit (or the track edges, if there's nothing but grass or gravel). This must be fixed. Why? The AI will drive without hesitation on any area within the corridors (e.g. to attempt an overtake) but you do not want the AI racing on many of these tarmac run-off areas. 

For example, when racing CART open-wheelers on a big oval like Michigan, you do not want the AI dipping off the banking onto the apron to attempt to overtake mid-corner – but as you can see in [this GPLaps video in AMS 1](https://www.youtube.com/watch?v=d5R6fFz3Rdw), suboptimal AIW corridors at Michigan lead to exactly this.

So be sure to adjust corridors accordingly!



### Track limits

Be careful to verify that cut detection areas (i.e. track limits) are reasoably positioned around the circuit via beta testing, and modify accordingly if required. Many isiMotor2 mod tracks have suffered from poorly set track limits... don't let your mod be another.




[^1]: From Speednut357 [in this RaceDepartment post](https://www.racedepartment.com/threads/modifying-the-aiw-file-to-reduce-first-corner-pileups.216017/).
[^2]: From Speednut357 [in this RaceDepartment post](https://www.racedepartment.com/threads/modifying-the-aiw-file-to-reduce-first-corner-pileups.216017/#post-3475611).
[^3]: [From Maxell on RaceDepartment](https://www.racedepartment.com/threads/aiw-changes-green-flag-location-corridors-pace-lap-lines.216192/#post-3474167).
