# Guide: tweaking SimBin AIW parameters



## From Shovas

The advice below is copied and adapted from private communication with Shovas on RaceDepartment.

### PLR file tuning

Before I tuned `RaceRatio`, I worked on global parameters in the PLR file like `AI Brake Grip Usage`, `AI Brake Power Usage` and `AI Corner Grip Usage` to alleviate the obvious problems of early braking or slow cornering speeds. 

### RaceRatio tuning

For tuning `RaceRatio`, I basically added 10 points to its default value (so if 1.0 then 1.1 ie. 110%), reloaded a saved race session for convenience where I qualified realistically at AI 100%, and observed how I performed compared to them: Was I able to keep up? Did I pass too easily? Did I fall behind too easily?

The idea being that I should be very close in performance to those AI I qualified close too.

If I passed too easily, I usually just added 10 again (ie. 1.2 or 120%). If I was too slow after that then I would subtract half again ie. -5 or 1.15 (or 115%). I tried doing a systematic bisection search of values from the start but after a while I found it got predictable and usually I could speed up the process by incrementing in 10 point increments. I did try to ensure that I was able to pass up to 5 cars in the first 10 minutes of a 20 minute race and maybe 5 more in the second half of the race. The AI should get harder and harder to catch up to and pass, of course, because they are really supposed to be better than you. If I passed more than that then I knew I had to increase `RateRatio` again. If I could pass anyone after about a lap, and I was falling behind, then I knew I had to decrease `RaceRatio`.

So, basically:

1. Qualify with at least 20 AI grid, AI 100, try to qualify top half but not too near the front, and save your session just before entering race so you can reload easily
2. Start race and see how they perform, whether passing too easily or falling behind too much
3. Adjust `RaceRatio` as needed and reload saved session and check their performance again

### Parameters to stay away from (in Shovas' opinion)

I tried tuning `AI Composure`, `AI to AI Collision Rate`, `AI Realism`, but found they weren't really all that effective. That's when I moved on to `RaceRatio`.

I stayed away from tweaking `QualRatio`. I've tried that before and it got overly complicated with too many variables to play with. So, this time around I wanted to simplify as much as possible and get as much value out of a single parameter as possible.

Same issue with `WorstAdjust`, `MidAdjust`, and `BestAdjust`. Before rebooting the mod I had played with them but as I recall it was overly complex to manage and didn't actually result in the real goal of making race performance more closely match qualifying performance. If I recall correctly, those parameters bring the whole pack closer together or further apart, i.e. the spread, but don't affect overall performance that you need to adjust to get them racing like they qualified.

### Other advice

Try to solve problems with universal parameters before diving into parameters you have to tune per track, per car, or per AI, for example. The combination explosion is just too complex. Just tuning a single parameter, `RaceRatio`, for each track, is work enough.

Where possible create a spreadsheet and do systematic bisection searches on your values so that you have an objective measure and less guesswork. That will help reveal patterns and make sure the values you're using really will tend to help universally not just in one particular instance. See the spreadsheet in the [SHO Competition AI mod](https://www.racedepartment.com/downloads/sho-competition-ai.30447/) for a sheet with nicely setup bisection search tables with precalculated numbers that do the math for you.



## From jgf

The advice below from jgf is copied and adapted from [this RaceDepartment thread](https://www.racedepartment.com/threads/new-simple-ai-experiment.211303/#post-3474480).

### AIW tweaking process for each track

Initially set `RaceRatio` and `QualRatio` to 1.000, and in-game AI to 100%. The ratios and `MidAdjust` at 1.000 equals 100%, so with in-game AI at 100% the AI is running unmodified at the settings of the AIW file. Changing the 1.000 is altering the percentage of teh AIW settings the AI will use, so at 0.950 they will run at 95% of the AIW setting, 1.05 would be 105% of the AIW settings.

Start by running enough practice laps to create a decent setup and know your average times. Run a few testing sessions and note the AI lap times, tweak MidAdjust to set them where you like. Now run a few qualifying sessions and tweak `QualRatio` to set the AI times where you want them. Next a few AI only races and tweak `RaceRatio` to set the laptimes there. You will now have AI speeds consistent through practice, qualifying, and race.

### Moving from track to track

Once `MidAdjust`, `RaceRatio` and `QualRatio` are set, move on to the next track, leaving AI in-game at 100%. When all tracks/mods are set (for now, lol, this is an ongoing process for me) the in-game AI can be tweaked for a particular race without upsetting the balance of practice/qualify/race speeds. Then it's a matter of fine tuning the talent files.

Start with default cars and tracks (I use the 360 Modena, easy to drive and setup; and a field of a dozen of them for the AI races). Now you're familiar with the procedure it's easy to repeat with any new track, 20-30 minutes of solo testing, 20-30 minutes of AI racing, and it's set (and I always spend at least an hour with a new track anyway, so it's not extra time involved).

### Different settings for different cars

The farther in performance a mod is from the default cars, the more likely it will require custom settings; though you may have enough leeway in the talent files to get decent performance with your base track settings.

### Clarification: practice speeds

`MidAdjust` is an overall setting, it will affect both ratio settings and practice speed; so set both ratios to 1.000 and tweak `MidAdjust` so the AI run the practice times you want, then use the ratio tweaks to set their qual and race times. Now when you vary the AI strength in the menu, all three will vary in proportion, AI will remain consistent through practice, qualifying, and race.

### Relation to AI setups

FWIW, you can then give the AI cars setups for each track (I use my own setups with a click or two taller top gear), then tweak the talent files so each performs slightly differently. An ongoing process, and excellent excuse for running more laps, but you'll be surprised how good AI races can be.


