# PLR file



## Parameters to tweak for improved AI behaviour

A big thank you to Shovas for his thorough investigation of these parameters.[^1]

### AI Limiter

- Range: 0.0 to 1.0
- The lower the value, the more AI cars will spread out[^2]
    - Higher value packs the AI together because the game will artificially make the AI drivers talents/speed closer
    - The spread is decided based on the talent files found in the car mod
- Set the AI Limiter variable to 0.0, and the AIs will always drive to the best of their ability...every lap[^3]

### AI Realism (i.e. aggression)

- Make the AI more aggressive, try to pass more
- Corresponding values in the GTR 2 UI
    - 0.70 = Timid (Min)
    - 0.85 = Clean
    - 1.0 = Real
    - 1.3 = Angry
    - 2.0 = Psychotic (Max)
- Note: in GTR2, this value is preserved in saved sessions
    - So your PLR value will be overwritten by the saved value if you continue a saved session

### AI Power Calibration

- Set to 0 to disallow game adjusting various available AI parameters (e.g. power, gearing, fuel) in order to get a baseline so AI calculations can be done reliably
- This may also be the parameter that causes AI to use less than full throttle for AI Strength % less than 95... more investigation required

### AI Additional Fuel Mult	

- Adjust to make sense of `AI Brake Power Usage`, `AI Brake Grip Usage` and `AI Corner Grip Usage`

### AI Brake Power Usage

- Increase (decrease) when AI brake too early (too late) before corners

### AI Brake Grip Usage

- Increase (decrease) when AI brake too early (too late) before corners

### AI Corner Grip Usage

- Increase (decrease) when AI are slower (faster) around corners than they should be

### AI Max Load

- Reportedly affects AI aggression somewhat... more investigation required

### Vehicle Specific AI Setups

- Reduce testing variables by avoiding custom AI setups (0)

### AI to AI Collision Rate

- Default value: 20
- Some users report increased AI situational awareness, improved collision detection, better overtaking with `AI to AI Collision Rate="40"`
    - In particular, it *should* help the AI to collide less
    - But in practice, some users say it makes little difference or leads to worse AI behaviour (e.g. weaving)



## Default GTR 2 PLR values

```json
AI Brake Power Usage="0.98000"
AI Brake Grip Usage="0.97000"
AI Corner Grip Usage="0.93000"
AI Realism="1.0"
AI Max Load="40000.00000"
AI to AI Collision Rate="20"
```



[^1]: Unless otherwise specified, information on this page is sourced from the README for [Shovas' SHO Competition AI mod](https://www.racedepartment.com/downloads/sho-competition-ai.30447/) for GTR 2.
[^2]: From Bela Valko in [this RaceDepartment post about rFactor 2](https://www.racedepartment.com/threads/how-to-fix-rf2-stock-car-rules.219153/page-2#post-3572368).
[^3]: From [this post about rFactor 2 from the Studio 397 forums](https://forum.studio-397.com/index.php?threads/rcd-talent-file.52996/) (because of rF2's roots in isiMotor2).

