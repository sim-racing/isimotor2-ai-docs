# Welcome!

## A community-made website for information about AI in isiMotor2 games

This is a community documentation website to provide an up-to-date all-in-one source of information and guidance about the AI of sim racing titles built on isiMotor2 – most of all, [rFactor 1](https://store.steampowered.com/app/339790/rFactor/), [GTR 2](https://store.steampowered.com/app/8790/GTR_2_FIA_GT_Racing_Game/), and [Automobilista 1](https://store.steampowered.com/app/431600/automobilista). 

For years, information about AI settings and parameters has been scattered across PDF documents and forum threads or hidden in the heads of experienced modders. This site hopes to change this by having as much knowledge as possible in a single easy-to-access location.

## This is an ongoing work in progress!

Please note that **this site is a work-in-progress**. Many pages on the site are currently incomplete, and more pages will likely need to be created.

## Contributing to the site

Community fixes, improvements, additions, and suggestions are welcome! That's the beauty of an open-source website like this one. We'd love to have you share your feedback or chip in.

There are a few ways to do this:

- [Open an issue](https://gitlab.com/sim-racing/isimotor2-ai-docs/-/issues) on [GitLab](https://gitlab.com/sim-racing/isimotor2-ai-docs/)
- [Merge request](https://gitlab.com/sim-racing/isimotor2-ai-docs/-/merge_requests) on [GitLab](https://gitlab.com/sim-racing/isimotor2-ai-docs/)
- Post on RaceDepartment in the [rFactor 1](https://www.racedepartment.com/forums/rfactor.10/), [GTR 2](https://www.racedepartment.com/forums/gtr-2.124/), or [Automobilista 1](https://www.racedepartment.com/forums/automobilista.405/) forums 
    - Would be most helpful to @ the website maintainer, MJQT
- Send MJQT a direct message on [RaceDepartment](https://www.racedepartment.com/)
